class MovieController < ApplicationController
  def index
  end

  def search
  	query = params[:search]

  	#Get the first page of results and the pagination info
  	current_page = 1
  	result = get_movies_by_title(query, current_page) 
  	@titles = Array.new
  	get_titles(result, @titles)
	total_result_pages = result['total_pages']

	#Check if there other pages and get it, if it exists
  	while current_page < total_result_pages
  		current_page = current_page + 1
  		result = get_movies_by_title(query, current_page) 
  		get_titles(result, @titles)
  	end	

  	@titles = @titles.sort

  end

  def get_titles(movie_data, titles_array)
  	movies = movie_data['data']
  	movies.each do |movie|
  		titles_array.push movie['Title']
  	end
  end

  def get_movies_by_title(query, page)
  	uri_string = 'https://jsonmock.hackerrank.com/api/movies/search/?Title='
  	uri_string.concat(query)
  	uri_string.concat('&page=')
  	uri_string.concat(page.to_s)
  	uri = URI(uri_string)
  	result = Net::HTTP.get(uri) 

  	return JSON.parse(result)
  end

end
